//Variables utilizadas
var filas: [Int] = []
var columnas: [Int] = []
var usuario: String?
var numero = 0
var correcto = false
var limite: Int
var resta: Int

func chequeo(_ maximo: Int) {       //Función que checa si el valor escrito
    usuario = readLine()            //es un número dentro del rango
    numero = Int(usuario!) ?? 0
    correcto = false
    while !correcto {
        if numero > 0 && numero < maximo {
            correcto = true
        } else {
            correcto = false
            print("Porfavor ingrese un numero entero positivo en el rango disponible")
            usuario = readLine()!
            numero = Int(usuario!) ?? 0
        }
    }
}

//Instrucciones
print("Ejercicio 1")
print("")
print("En este ejercicio el usuario me proporcionará el tamaño de una matriz. Comenzando con el primer recuadro (0,0) mirando hacia la derecha seguiré en esa dirección hasta que tope con un borde o con un recuadro que ya haya pasado, al final le arrojaré la dirección en la que termino mirando (R: Derecha, L: Izquierda, U: Arriba, D: Abajo). Dependiendo de las pruebas que quiera le proporcionaré los espacios necesarios.")

print("")
print("¿Cuántas pruebas quiere hacer? (1...5000)")

//Asigna la cantidad de pruebas solicitadas
chequeo(5000)
limite = numero

print("")
print("Ingrese el numero de las Columnas y Filas (1...10^9)")

for i in 0..<limite { //Asigna el valor de las C y F
    print("Caso \(i + 1)")
    print("Columna: ")
    chequeo(1000000000)
    columnas.append(numero)
    print("Fila:")
    chequeo(1000000000)
    filas.append(numero)
}

print("")

for i in 0..<limite { //Devuelve L R U D de acuerdo al patron encontrado
    resta = columnas[i] - filas[i]
    if resta == 0 {
        if columnas[i] % 2 == 0 {
            print("\(i + 1): L")
        } else {
            print("\(i + 1): R")
        }
    } else if resta < 0 {
        if columnas[i] % 2 == 0 {
            print("\(i + 1): U")
        } else {
            print("\(i + 1): D")
        }
    } else {
        if filas[i] % 2 == 0 {
            print("\(i + 1): L")
        } else {
            print("\(i + 1): R")
        }
    }
    print("")
}
